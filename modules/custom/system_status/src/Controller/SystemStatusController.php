<?php

/**
 * @file
 * Contains \Drupal\pants\Controller\SystemStatusController.
 */

namespace Drupal\system_status\Controller;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\system_status\Services\SystemStatusEncryption;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Returns responses for Sensei's Pants routes.
 */
class SystemStatusController extends ControllerBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $module_handler;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $theme_handler;

  /**
   * The system status encrypt service
   *
   * @var \Drupal\system_status\Services\SystemStatusEncryption
   */
  protected $encrypt;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('theme_handler'),
      $container->get('system_status.encrypt')
    );
  }

  /**
   * SystemStatusController constructor.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   * @param \Drupal\system_status\Services\SystemStatusEncryption $encrypt
   */
  public function __construct(ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler, SystemStatusEncryption $encrypt) {
    $this->module_handler = $module_handler;
    $this->theme_handler = $theme_handler;
    $this->encrypt = $encrypt;
  }

  /**
   * Changes Sensei's pants and returns the display of the new status.
   */
  function load($system_status_token) {

    $res = [];
    if ($available = update_get_available(TRUE)) {
      \Drupal::moduleHandler()->loadInclude('update', 'update.compare');
      $res = update_calculate_project_data($available);
    }

    $config = $this->config('system_status.settings');
    if(function_exists('openssl_random_pseudo_bytes')) {
      $res = SystemStatusEncryption::encrypt_openssl(json_encode(["system_status" => $res]));
      return new JsonResponse(["system_status" => "encrypted_openssl", "data" => $res, "drupal_version" => "8", "engine_version" => "DRUPAL8"]);
    }
    else if (extension_loaded('mcrypt')) {
      $res = SystemStatusEncryption::encrypt_mcrypt(json_encode(["system_status" => $res]));
      return new JsonResponse(["system_status" => "encrypted", "data" => $res, "drupal_version" => "8", "engine_version" => "DRUPAL8"]);
    }
    else {
      return new JsonResponse(["system_status" => $res, "drupal_version" => "8", "engine_version" => "DRUPAL8"]);
    }
  }

  public function access($system_status_token) {
    $token = $this->config('system_status.settings')->get('system_status_token');
    if($token == $system_status_token) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::forbidden();
    }
  }

}
