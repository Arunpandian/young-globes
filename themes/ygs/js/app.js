
jQuery(document).ready(function ($) {
   $('.navbar-nav').attr('id', 'top-navigation');
    var lastId,
        topMenu = $("#navbar"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find('a[href^="#"]'),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function () {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        });

    //Get width of container
    var containerWidth = $('.section .container').width();
    //Resize animated triangle
    $(".triangle").css({
        "border-left": containerWidth / 2 + 'px outset transparent',
        "border-right": containerWidth / 2 + 'px outset transparent'
    });
    $(window).resize(function () {
        containerWidth = $('.container').width();
        $(".triangle").css({
            "border-left": containerWidth / 2 + 'px outset transparent',
            "border-right": containerWidth / 2 + 'px outset transparent'
        });
    });

    //Initial mixitup, used for animated filtering portfolio.
    $('ul.thumbnails').mixitup({
        'onMixStart': function (config) {
            $('div.toggleDiv').hide();
        }
    });

    $('input, textarea').placeholder();

    // Bind to scroll
    $(window).scroll(function () {

        //Display or hide scroll to top button 
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }

        if ($(this).scrollTop() > 130) {
            $('.navbar').addClass('navbar-fixed-top animated fadeInDown');
        } else {
            $('.navbar').removeClass('navbar-fixed-top animated fadeInDown');
        }

        // Get container scroll position
        var fromTop = $(this).scrollTop() + topMenuHeight + 10;

        // Get id of current scroll item
        var cur = scrollItems.map(function () {
            if ($(this).offset().top < fromTop)
                return this;
        });

        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .parent().removeClass("active")
                .end().filter("[href=#" + id + "]").parent().addClass("active");
        }
    });

	// Add jQuery One Page Nav Plugin
	$('.navbar-nav').onePageNav({
		 currentClass: 'active',
		 changeHash: false,
		 scrollSpeed: 1200
	});

    /*
    Function for scrolling to top
    ************************************/
    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });


    $(window).load(function () {
        function filterPath(string) {
            return string.replace(/^\//, '').replace(/(index|default).[a-zA-Z]{3,4}$/, '').replace(/\/$/, '');
        }
        $('a[href*=#]').each(function () {
            if (filterPath(location.pathname) == filterPath(this.pathname) && location.hostname == this.hostname && this.hash.replace(/#/, '')) {
                var $targetId = $(this.hash),
                    $targetAnchor = $('[name=' + this.hash.slice(1) + ']');
                var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;

                if ($target) {

                    $(this).click(function () {

                        //Hack collapse top navigation after clicking
                        topMenu.parent().attr('style', 'height:0px').removeClass('in'); //Close navigation
                        $('.navbar .btn-navbar').addClass('collapsed');

                        var targetOffset = $target.offset().top - 63;
                        $('html, body').animate({
                            scrollTop: targetOffset
                        }, 800);
                        return false;
                    });
                }
            }
        });
    });



    //Function for show or hide portfolio description.
    $.fn.showHide = function (options) {
        var defaults = {
            speed: 1000,
            easing: '',
            changeText: 0,
            showText: 'Show',
            hideText: 'Hide'
        };
        var options = $.extend(defaults, options);
        $(this).click(function () {
            $('.toggleDiv').slideUp(options.speed, options.easing);
            var toggleClick = $(this);
            var toggleDiv = $(this).attr('rel');
            $(toggleDiv).slideToggle(options.speed, options.easing, function () {
                if (options.changeText == 1) {
                    $(toggleDiv).is(":visible") ? toggleClick.text(options.hideText) : toggleClick.text(options.showText);
                }
            });
            return false;
        });
    };

    //Initial Show/Hide portfolio element.
    $('div.toggleDiv').hide();
    $('.show_hide').showHide({
        speed: 500,
        changeText: 0,
        showText: 'View',
        hideText: 'Close'
    });

    /************************
    Animate elements
    *************************/
    $('.contact-form').addClass('col-md-5 col-xs-12');

    //Animate thumbnails 
    jQuery('.view-id-portfolio .thumbnail').one('inview', function (event, visible) {
      if (visible == true) {
        $('.view-id-portfolio .thumbnail').addClass("animated fadeInDown");
      }
      else {
        $('.view-id-portfolio .thumbnail').removeClass("animated fadeInDown");
      }
    });

    jQuery('.view-about-us .thumbnail').one('inview', function (event, visible) {
      if (visible == true) {
        $('.view-about-us .thumbnail').addClass("animated fadeInDown");
      }
      else {
        $('.view-about-us .thumbnail').removeClass("animated fadeInDown");
      }
    });


    //Animate triangles
    jQuery('.triangle').bind('inview', function (event, visible) {
        if (visible == true) {
            jQuery(this).addClass("animated fadeInDown");
        } else {
            jQuery(this).removeClass("animated fadeInDown");
        }
    });
    
    //animate first team member
    jQuery('#first-person').bind('inview', function (event, visible) {
        if (visible == true) {
            jQuery('#first-person').addClass("animated pulse");
        } else {
            jQuery('#first-person').removeClass("animated pulse");
        }
    });
    
    //animate second team member
    jQuery('#second-person').bind('inview', function (event, visible) {
        if (visible == true) {
            jQuery('#second-person').addClass("animated pulse");
        } else {
            jQuery('#second-person').removeClass("animated pulse");
        }
    });

    //animate third team member
    jQuery('#third-person').bind('inview', function (event, visible) {
        if (visible == true) {
            jQuery('#third-person').addClass("animated pulse");
        } else {
            jQuery('#third-person').removeClass("animated pulse");
        }
    });
    
    //Animate blog columns
    jQuery('.timeline-image, .timeline-panel, .testimonial').bind('inview', function (event, visible) {
        if (visible == true) {
            jQuery(this).addClass("animated fadeInDown");
        } else {
            jQuery(this).removeClass("animated fadeInDown");
        }
    });

    //Animate contact form
    jQuery('.contact-form').bind('inview', function (event, visible) {
        if (visible == true) {
            jQuery('.contact-form').addClass("animated bounceIn");
        } else {
            jQuery('.contact-form').removeClass("animated bounceIn");
        }
    });

    //Animate skill bars
    jQuery('.skills li span.bar').one('inview', function (event, visible) {
        if (visible == true) {
            jQuery('.skills li span.bar').each(function () {
                jQuery(this).animate({
                    width: jQuery(this).attr('data-width')
                }, 3000);
            });
        }
    });
});

// Add Bootstrap specific functions and styling.
var Drupal = Drupal || {};

(function ($, Drupal, drupalSettings) {
  "use strict";

  /**
   * Bootstrap Tooltips.
   */
  Drupal.behaviors.appJS = {
    attach: function (context) {
      $('.navbar-nav li a').click(function(){
        var href = $(this).attr('href');
        console.log(href);
        window.location.replace(href);
    });
    $('.navbar-collapse li a').click(function() {
      $('.navbar-collapse').removeClass('in');
    });
    $('.da-slider').attr('id', 'da-slider');
    $('#da-slider').cslider();
	$('.comment-reply-link').addClass('btn btn-default');
	$('button[type=submit]').addClass('btn btn-default');
	$('#submit, html input[type=button], input[type=reset], input[type=submit]').addClass('btn btn-default');
	$('.widget_rss ul').addClass('media-list');
	$('select').addClass('form-control');
	$('table#wp-calendar').addClass('table table-striped');
	$('.tagcloud a').addClass('btn btn-default btn-xs');
	$('#submit, .tagcloud, button[type=submit], .comment-reply-link, .widget_rss ul, .select, table#wp-calendar').show("fast");
    $('.box').hover(
        function () {
            $('.box').removeClass('active');
            $(this).addClass('active');
    }, function(){
        $(this).removeClass('active');
    });
    }
  };

})(jQuery, Drupal, drupalSettings);